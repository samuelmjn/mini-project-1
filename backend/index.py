# -*- coding: utf-8 -*-

from graphqlclient import GraphQLClient
import json
from BaseHTTPServer import HTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler
import cgi

client = GraphQLClient('https://api.github.com/graphql')
client.inject_token('Bearer 326f904137b16795b047eb43b61edd5ce945fe60')

results = client.execute('''
{
  repository(name: "react", owner: "facebook") {
    issues (last: 20){
      nodes {
        number 
        title
        comments (last: 3){
          nodes {
            body
            author{
              url
            }
          }
        }
        author {
          url
        }
      } 
    }
  }
}
''')

issuesObject = json.loads(results)
issuesList = issuesObject["data"]["repository"]["issues"]["nodes"]

array = {}
number = -1

for issues in issuesList:
    number += 1
    author = issues["author"]["url"]
    id = issues["number"]
    title = issues["title"]

    array[number] = {}
    array[number]["author"] = author
    array[number]["id"] = id
    array[number]["title"] = title

    array[number]["comments"] = {}
    issueComments = issues["comments"]["nodes"]
    count = -1

    for comments in issueComments:
        count += 1
        array[number]["comments"][count] = {}
        commentauthor = comments["author"]["url"]
        commentbody = comments["body"]
        array[number]["comments"][count]["author"] = commentauthor
        array[number]["comments"][count]["body"] = commentbody

issueJson = json.dumps(array)


class RestHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(issueJson)
        return


httpd = HTTPServer(('0.0.0.0', 8080), RestHTTPRequestHandler)
while True:
    print('Server running on port 8080!')
    httpd.handle_request()
